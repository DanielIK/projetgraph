#include "grman/grman.h"
#include "Models/Sommet.h"
#include <iostream>

int main()
{
    // initialisation d'allegro et widget
    grman::init();

    // le nom du repertoire ou se trouve les assets
    grman::set_pictures_path("Assets");

    Sommet s("Foret/Deer@big.bmp", 30,30,SOMMETWIDE,SOMMETHEIGH);

    // Boucle de Jeu
    while( !key[KEY_ESC] ){

        s.m_interface->update();

        grman::mettre_a_jour();

    }

    grman::fermer_allegro();

    return 0;
}
END_OF_MAIN();
