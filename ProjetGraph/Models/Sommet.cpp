#include "Sommet.h"

/****************************************/
/**         GraphicSommet              **/
/****************************************/

GraphicSommet::GraphicSommet(std::string bmpFileName, double x, double y, double w, double h) {

    // Setup top Box
    m_top_box.set_pos(x,y);
    m_top_box.set_dim(w,h);
    m_top_box.set_moveable(true);
    m_top_box.set_bg_color(GRISSOMBRE);

    // Setup image*
    m_top_box.add_child(m_img);
    m_img.set_pic_name(bmpFileName);
    m_img.set_gravity_x(grman::GravityX::Center);
    m_img.set_posy(5);

}

/*---------------------------------------------*/

void GraphicSommet::update() {

    this->m_top_box.update();
}

/****************************************/
/**             Sommet                 **/
/****************************************/

Sommet::Sommet(std::string bmpFileName, double x, double y, double w, double h)
{
    m_interface = new GraphicSommet(bmpFileName,x,y,w,h);
}

void Sommet::creation_aretes()
{
    for(unsigned int i = 0 ; i < m_in.size() ; i++)
    {
        m_aretes.push_back(new Arete(m_in[i],this));
    }

    for(unsigned int i = 0 ; i < m_out.size() ; i++)
    {
        m_aretes.push_back(new Arete(this,m_out[i]));
    }
}
