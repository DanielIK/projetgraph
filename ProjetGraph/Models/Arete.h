#ifndef ARETE_H_INCLUDED
#define ARETE_H_INCLUDED

// Biblioth�ques Standards
#include <string>
#include <vector>
#include <memory>

// Biblioth�que Perso
#include "../grman/grman.h"
//#include "Sommet.h"

// D�finition des constantes


/****************************************/
/**         GraphicArete               **/
/****************************************/

class GraphicArete {

friend class Arete ;
friend class Sommet;

private :

    ///Le 'corps' de l'arete
    grman::WidgetEdge m_top_edge;

    ///Texte affichant le poids
    grman::WidgetText m_label_weight;



public:
    /** CONTRUCTEUR **/
    GraphicArete(Sommet* depart, Sommet* arrive);

    /** GETTERS / SETTERS **/
    grman::WidgetEdge& get_top_edge() {return m_top_edge;}

    /** PUBLICS METHODES **/

};

/****************************************/
/**              Arete                 **/
/****************************************/

class Arete {

friend class GraphicArete ;
friend class Sommet;

private:
    ///Sommet � la source
    Sommet* m_depart;

    ///Sommet de destination
    Sommet* m_arrivee;

    int m_poids;

    GraphicArete* m_interface = nullptr ;
public :


    /** CONTRUCTEUR **/
    Arete(Sommet* depart, Sommet* arrive);
};

#endif // ARETE_H_INCLUDED
