#ifndef SOMMET_H_INCLUDED
#define SOMMET_H_INCLUDED

// Bibliothèques Standards
#include <string>
#include <vector>
#include <memory>

// Bibliothèque Perso
#include "../grman/grman.h"
//#include "Arete.h"

// Définition des constantes
#define SOMMETWIDE 150      /// Largeur top_box d'un sommet
#define SOMMETHEIGH 200     /// Hauteur top_box d'un sommet

/****************************************/
/**         GraphicSommet              **/
/****************************************/

class GraphicSommet {

friend class Sommet;
friend class Arete;

private:

    ///La boite contenant le tout
    grman::WidgetBox m_top_box;

    ///L'image associée au sommet
    grman::WidgetImage m_img;

public:

    /** GETTERS / SETTERS **/
     grman::WidgetBox& get_top_box() {return m_top_box;}

    /** CONTRUCTEUR **/
    GraphicSommet(std::string bmpFileName, double x, double y, double w, double h);


    /** PUBLICS METHODES **/
    void update();

};

/****************************************/
/**             Sommet                 **/
/****************************************/

class Sommet {

friend class Arete;
friend class GraphicSommet;


private:

    std::string m_nom;

    std::vector<Sommet*> m_in;  // vecteur de pointeur sur les predecesseurs
    std::vector<Sommet*> m_out; // vecteur de pointeur sur les successeurs

    std::vector<Arete*> m_aretes;

    GraphicSommet* m_interface = nullptr ;

public:

    void creation_aretes();

    Sommet(std::string bmpFileName, double x, double y, double w, double h);
};

#endif // SOMMET_H_INCLUDED
