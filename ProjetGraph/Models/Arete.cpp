#include "Arete.h"

/****************************************/
/**         GraphicArete               **/
/****************************************/

GraphicArete::GraphicArete(Sommet* depart, Sommet* arrive)
{
    //si les sommets existent ?
    if (!(depart->m_interface && arrive->m_interface))
    {
        std::cout << "ERREUR GRAPHIC ARETE" ;
    }

    //Setup top edge
    m_top_edge.attach_from(depart->m_interface->get_top_box());
    m_top_edge.attach_to(arrive->m_interface->get_top_box());
    m_top_edge.reset_arrow_with_bullet();

    //Setup poids
    m_top_edge.add_child( m_label_weight );
    m_label_weight.set_gravity_y(grman::GravityY::Down);

}

/****************************************/
/**              Arete                 **/
/****************************************/

Arete::Arete(Sommet* depart, Sommet* arrive)
{
    m_depart=depart;
    m_arrivee=arrive;

    m_interface = new GraphicArete(depart,arrive);
}
